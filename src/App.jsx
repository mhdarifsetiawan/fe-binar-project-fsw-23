import { Route, Routes } from "react-router-dom";
import Registration from "./pages/Registration";
import Login from "./pages/auth/Login";
import Forgot from "./pages/Forgot";
import CreateGame from "./pages/game/create";
import Game from "./pages/game/game";
import Dashboard from "./pages/dashboard/index";
import Profile from "./pages/dashboard/Profile";
import LandingPage from "./pages/LandingPage";
import Gamelist from "./pages/listgame/Gamelistnew";
import NumberGuessing from "./pages/game/numberguess";
import SinglePlayer from "./pages/game/singlePlayer";
import Hangman from "./pages/game/hangman";
import Rps from "./pages/game/Rps";
import Achievement from "./pages/dashboard/achievement";
import Leaderboard from "./pages/Leaderboard";
import EditProfile from "./pages/dashboard/EditProfile";
import EditPassword from "./pages/dashboard/EditPassword";
import FriendsList from "./pages/dashboard/FriendsList";

const App = (props) => {
  return (
    <>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/leaderboard" element={<Leaderboard />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="/login" element={<Login />} />
        <Route path="/forgot-password" element={<Forgot />} />
        <Route path="/dashboard/profile" element={<Profile />} />
        <Route path="/game/single" element={<SinglePlayer />} />
        <Route path="/create" element={<CreateGame />} />
        <Route path="/game/:roomId" element={<Game />} />
        <Route path="/game/list" element={<Gamelist />} />
        <Route path="/game/single/number" element={<NumberGuessing />} />
        <Route path="/game/single/hangman" element={<Hangman />} />
        <Route path="/game/single/rps" element={<Rps />} />
        <Route path="/achievement" element={<Achievement />} />
        <Route path="/dashboard/profile/edit" element={<EditProfile />} />
        <Route path="/dashboard/password/edit" element={<EditPassword />} />
        <Route path="/dashboard/friendlist" element={<FriendsList />} />
      </Routes>
    </>
  );
};

export default App;
