import axios from "axios";
import { React } from "react";
import { useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate, Link } from "react-router-dom";
import Button from "../components/_button";

import "../styles/main.css";
import "../styles/registration.css";
import "../styles/form.css";
import "../styles/_button.css";

const Registration = () => {
  const [values, setValues] = useState({});
  const navigate = useNavigate();

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/user/registration",
        values
      )
      .then((res) => navigate("/login"))
      .catch((err) => {
        alert("something wrong!");
      });
  };

  return (
    <div className="content">
      <div className="registration-navbar">
        <nav class="navbar navbar-expand-lg bg-light">
          <div class="container-fluid">
            <Link className="navbar-brand" to="/">
              Titik Koma
            </Link>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <Link className="nav-link" to="/registration">
                    Registration
                  </Link>
                </li>
                <li class="nav-item">
                  <Link className="nav-link" to="/login">
                    Login
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-4 m-auto bg-white p-4">
            <span className="form-title">Registration Form</span>
            <form onSubmit={handleSubmit}>
              <div class="mb-3">
                <label for="fullname" class="form-label">
                  Full name
                </label>
                <input
                  type="text"
                  class="form-control"
                  id="fullname"
                  name="fullname"
                  required
                  onChange={handleChange}
                />
              </div>
              <div class="mb-3">
                <label for="email" class="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  class="form-control"
                  id="email"
                  name="email"
                  aria-describedby="emailHelp"
                  required
                  onChange={handleChange}
                />
                <div id="emailHelp" class="form-text">
                  We'll never share your email with anyone else.
                </div>
              </div>
              <div class="mb-3">
                <label for="password" class="form-label">
                  Password
                </label>
                <input
                  type="password"
                  class="form-control"
                  id="password"
                  name="password"
                  onChange={handleChange}
                />
              </div>
              <div class="container-form-btn">
                <div class="wrap-form-btn">
                  <div class="form-bgbtn"></div>
                  <Button className="form-btn" text="Registration"></Button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Registration;
