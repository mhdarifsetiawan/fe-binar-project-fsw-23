import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import _navbarLanding from "../components/_navbarLanding";
import "../styles/landingPage.css";
import Button from "../components/_button";

const LandingPage = () => {
  const [leaders, setLeaders] = useState([]);

  const fetchLeaders = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/users?pageNumber=1&limitParm=3"
      )
      .then((res) => {
        const listLeaders = res.data;
        setLeaders(listLeaders);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    fetchLeaders();
  }, []);

  return (
    <div className="main">
      <_navbarLanding></_navbarLanding>
      <div className="section-1 d-flex">
        <div className="hero-container w-100 h-auto d-flex flex-column justify-content-end">
          <div className="row h-100">
            <div className="col-12 my-auto section-1_left text-center">
              <h2>PLAY TRADITIONAL GAME</h2>
              <h3 class="text-white hero-title mb-4">
                Experience new traditional game play
              </h3>
              <div class="container-form-btn">
                <div class="wrap-form-btn w-auto m-auto">
                  <div class="form-bgbtn"></div>
                  <Link to="/login">
                    <Button className="form-btn" text="Play Now"></Button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="content-2 text-dark">
            <div className="row">
              <div className="col-lg-4">
                <div className="wrapper d-flex w-100 h-100 d-flex flex-column">
                  <h2>ABOUT</h2>
                  <div
                    className="text-white fw-normal"
                    style={{ fontFamily: "sans-serif" }}
                  >
                    Rock-Paper-Scissors is a two-person hand game. This game is
                    often used for random selection, as well as tossing coins,
                    dice, and more. Among Indonesian children, this game is also
                    known as "Japanese Suwit".
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="wrapper d-flex w-100 h-100 d-flex flex-column">
                  <h2>FEATURES</h2>
                  <ul>
                    <li>
                      <div className="text-warning">Traditional Game</div>
                      <div
                        className="text-white fw-normal"
                        style={{ fontFamily: "sans-serif" }}
                      >
                        if you miss your childhood. We provide many traditional
                        games here
                      </div>
                    </li>
                    <li>
                      <div className="text-warning">Player vs Player</div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="wrapper d-flex w-100 h-100 d-flex flex-column">
                  <h2>LEADERBOARD</h2>
                  <div>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Score</th>
                        </tr>
                      </thead>
                      <tbody className="text-capitalize">
                        {leaders.map((leader, index) => {
                          return (
                            <tr>
                              <th scope="row">{index + 1}</th>
                              <td>{leader.fullname}</td>
                              <td>{leader.score}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer>
        <div className="footer m-auto text-dark">
          <Link to="/">&copy; 2022 - Titik Koma</Link>
        </div>
      </footer>
    </div>
  );
};

export default LandingPage;
