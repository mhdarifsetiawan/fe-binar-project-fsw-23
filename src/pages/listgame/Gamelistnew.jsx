import React, { useState, useEffect } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Cardgamelist from "./Cardgamelist";
import Navbar from "../../components/_navbar";
import { useParams } from "react-router-dom";

const Gamelistnew = () => {
  const [cookies, setCookies] = useCookies([
    "accessToken",
    "id",
    "loginStatus",
  ]);

  const { id } = useParams();
  // console.log("test");

  const [countOne, setCountOne] = useState(1);
  const [countTwo, setCountTwo] = useState(2);
  const [countThree, setCountThree] = useState(3);

  // console.log(cookies.accessToken);

  const [rooms, setRooms] = useState([]);

  const navigate = useNavigate();

  const fetchPosts = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/game/list?pageNumber=${countOne}`
      )
      .then((res) => {
        const gamelist = res.data;
        setRooms(gamelist);
        // console.log(gamelist);
      })
      .catch((err) => console.error(err));
  };

  const addCounting = (countOne, countTwo, countThree) => {
    setCountOne(countOne + 1);
    setCountTwo(countTwo + 1);
    console.log(countOne, "asdasd");
    setCountThree(countThree + 1);
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/game/list?pageNumber=${countTwo}`
      )
      .then((res) => {
        const gamelist = res.data;
        setRooms(gamelist);
        console.log(gamelist);
      })
      .catch((err) => console.error(err));
  };

  const lessCounting = (countOne, countTwo, countThree) => {
    if (countOne > 1) {
      setCountOne(countOne - 1);
      setCountTwo(countTwo - 1);
      setCountThree(countThree - 1);
      axios
        .get(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/game/list?pageNumber=${countTwo - 2}`
        )
        .then((res) => {
          const gamelist = res.data;
          setRooms(gamelist);
        })
        .catch((err) => console.error(err));
    }
  };

  const handleClick = (id, roomName) => {
    navigate(`/game/${id}`);
    console.log(id);
  };

  const handleHover = () => {
    this.setState({
      isHovered: !this.state.isHovered,
    });
  };

  useEffect(() => {
    fetchPosts();
    // console.log(cookies.accessToken);
  }, []);
  return (
    <>
      <Navbar></Navbar>
      <div
        className="container"
        style={{
          width: "fit-content",
          justifyContent: "space-around",
          marginTop: "60px",
          display: "flex",
          flexWrap: "wrap",
          marginLeft: "22%",
        }}
      >
        {rooms.map((room) => (
          <Cardgamelist
            key={room.id}
            name={room.name}
            description={room.description}
            winner={room.winner}
            onClick={() => handleClick(room.id, room.name)}
            onMouseEnter={() => handleHover()}
          />
        ))}
        <div
          className="container pagination"
          style={{
            height: "40px",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          <center>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <li class="page-item">
                  <a
                    class="page-link"
                    href="#"
                    aria-label="Previous"
                    onClick={() => lessCounting(countOne, countTwo, countThree)}
                  >
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    {countOne}
                  </a>
                </li>
                <li class="page-item">
                  <a
                    class="page-link"
                    href="#"
                    aria-label="Next"
                    onClick={() => addCounting(countOne, countTwo, countThree)}
                  >
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </center>
        </div>
      </div>
    </>
  );
};

export default Gamelistnew;
