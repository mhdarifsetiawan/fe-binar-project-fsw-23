import React from "react";
import { motion } from "framer-motion";

const Cardgamelist = (props) => {
  const { name, description, winner, onClick } = props;
  return (
    <motion.div exit={{ opacity: 0 }}>
      <div onClick={onClick}>
        <div class="row mt-4" style={{ width: "600px" }}>
          <div class="col mt-4">
            <div class="card h-100">
              <img
                src="https://raw.githubusercontent.com/QuentinLucyyd/Rock-Paper-Scissors/master/public/assets/Screenshot-1.jpg"
                class="card-img-top"
                alt="rock paper scissor.jpg"
              />
              <div class="card-footer">
                  <small ><h1 style={{textAlign:"center"}}>{name}</h1></small>
                </div>
              <div class="card-body ">
                <h2 style={{fontSize:"20px", fontWeight:"bold"}}>Description:</h2>
                <p class="card-text" style={{fontSize:"20px"}}>{description}</p>
              </div>
              <div class="card-footer">
                <small class="text-muted" value="Winner Is">
                  <p style={{height:"25px", paddingTop:"10px"}}>Winner is: <span style={{position:"relative", display:"flex", justifyContent: "center", alignItems: "center", fontSize:"30px", top:"-34px"}}>{winner}</span></p>
                  
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </motion.div>

    // {/* <div style={styles.card} onClick={onClick}> */}
    //   <img
    //     src="https://raw.githubusercontent.com/QuentinLucyyd/Rock-Paper-Scissors/master/public/assets/Screenshot-1.jpg"
    //     className=" card-img-top img-fluid rounded-3"
    //     alt="rock paper scissor.jpg"
    //   />
    //   <div style={styles.title}>{name}</div>
    //   <h3
    //     style={{
    //       margin: "0",
    //       marginTop: "20px",
    //       fontSize: "20px",
    //       fontWeight: "bold",
    //     }}
    //   >
    //     Description:
    //   </h3>
    //   <div style={styles.description}>{description}</div>
    //   <h4 style={{ textAlign: "center" }}>Winner is</h4>
    //   <div style={styles.winner}>{winner}</div>
    //   <div></div>
  );
};

// const styles = {
//   // card: {
//   //   border: "0px solid black",
//   //   width: "400px",
//   //   margin: "35px",
//   //   borderRadius: "15px",
//   //   backgroundColor: "white",
//   // },
//   title: {
//     display: "flex",
//     fontWeight: "bold",
//     fontSize: "35px",
//     justifyContent: "center",
//     alignItems: "center",
//     color: "black",
//   },
//   description: {
//     fontSize: "20px",
//     display: "flex",
//     color: "black",
//     marginBottom: "15px",
//   },
//   winner: {
//     fontWeight: "bold",
//     fontSize: "25px",
//     justifyContent: "center",
//     alignItems: "center",
//     display: "flex",
//     color: "black",
//   },
// };

export default Cardgamelist;
