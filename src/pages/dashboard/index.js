import { useCookies } from "react-cookie";
import "../../styles/dashboard.css";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";
import Navbar from "../../components/_navbar";
import axios from "axios";
import HelloMessage from "../../components/_helloMessage";

const Dashboard = () => {
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken", "fullname"]);
  const [score, setScore] = useState(0);
  let decoded = "";

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    } else {
      axios
        .get(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/user/${decoded.id}`,
          {
            headers: { Authorization: `Bearer ${cookies.accessToken}` },
          }
        )
        .then((res) => {
          setScore(res.data.score);
        })
        .catch((error) => console.log(error));
    }
  }, []);

  return (
    <>
      <Navbar />
      {/*Main Body*/}
      <main className="bg-dark navbar-expand-lg">
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Add Friend
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <form>
                <div className="modal-body">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="username"
                    aria-label="addUser"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Modal */}
        <div className="container-fluid">
          <HelloMessage></HelloMessage>
          <div className="row">
            <div className="my-4 col-md-8">
              <div>
                <div className="circle-box position-relative top-50 start-40">
                  <p className="mb-0 fw-bold">Your</p>
                  <p className="fw-bold">Score</p>
                  <p className="fw-bolder fs-2">{score}</p>
                </div>
              </div>
              <img
                src="https://storage.googleapis.com/exstemsions/dice-header.jpg?mtime=20180307213620"
                className="card-img-top rounded-5 my-3"
                alt="dice.jpg"
              />
              <div className="text-bg-dark my-3">
                <h4>Your Most Played Game</h4>
                <div className="row">
                  <div className="col-md-6">
                    <div
                      className="card bg-tint border-0"
                      style={{ width: "18rem" }}
                    >
                      <div className="card-body">
                        <img
                          src="https://raw.githubusercontent.com/QuentinLucyyd/Rock-Paper-Scissors/master/public/assets/Screenshot-1.jpg"
                          className=" card-img-top img-fluid rounded-3"
                          alt="rock paper scissor.jpg"
                        />
                        <h5 className="card-title">Rock Paper Scissor</h5>
                        <p className="card-text small">
                          A game that hones your six sense. Create your own room
                          and challenge other player to play a game with you.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div
                      className="card bg-tint border-0"
                      style={{ width: "18rem" }}
                    >
                      <div className="card-body">
                        <img
                          src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//84/MTA-12176210/oem_mini-travel-games-tic-tac-toe-game-puzzle-game-educational-toys-for-kids_full05.jpg"
                          className=" card-img-top img-fluid rounded-3"
                          alt="tic tac toe.jpg"
                        />
                        <h5 className="card-title">Tic Tac Toe</h5>
                        <p className="card-text small">
                          Play one of the oldest traditional game on the go play
                          with computer or with any user online.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row my-2">
                  <div className="col-md-6">
                    <div
                      className="card bg-tint border-0"
                      style={{ width: "18rem" }}
                    >
                      <div className="card-body">
                        <img
                          src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/fcf0e762143121.5a861ac8abf27.png"
                          className=" card-img-top img-fluid rounded-3"
                          alt="pubg.jpg"
                        />
                        <h5 className="card-title">
                          Playerunknow's BattleGround
                        </h5>
                        <p className="card-text small">
                          Play the most popular battle royale game solo or with
                          your squad.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div
                      className="card bg-tint border-0"
                      style={{ width: "18rem" }}
                    >
                      <div className="card-body">
                        <img
                          src="https://staticg.sportskeeda.com/editor/2022/09/fc20f-16625842153788-1920.jpg"
                          className=" card-img-top img-fluid rounded-3"
                          alt="monster hunter rise.jpg"
                        />
                        <h5 className="card-title">
                          Monster Hunter Rise : Sunbreak
                        </h5>
                        <p className="card-text small">
                          Hunt all the new monster and other subspecies in the
                          new expansion from Monster Hunter Rise <sup>TM</sup>{" "}
                          solo or wtih friends.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-3 offset-md-1">
              <div className="bg-grey card h-50 border-3 text-bg-dark">
                <img
                  className="rounded-circle image-fluid  h-25 mx-auto d-block my-3"
                  src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png"
                  alt="profile pic.jpg"
                />
                <div className="wy-2 text-center fs-1">{cookies.fullname}</div>
                <hr />
                <ul className="px-2 list-unstyled">
                  <li>
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#onlineCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="onlineCollapse"
                    >
                      <i className="bi bi-dot text-success fs-3" /> Online{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="onlineCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row">
                          <li className="small fw-bold my-1 px-1">Friend 1</li>
                          <li className="small fw-bold my-1 px-1">Friend 2</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li className="wy-2 fw-bold px-2">
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#chatCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="chatCollapse"
                    >
                      <i className="bi bi-chat-dots me-2" /> Chat{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="chatCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row"></ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <img className="img-fluid w-50" src="" alt="pubg header" />
            </div>
          </div>
        </div>
      </main>
      {/*Main Body*/}
    </>
  );
};

export default Dashboard;
