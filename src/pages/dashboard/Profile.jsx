import { useCookies } from "react-cookie";
import "../../styles/dashboard.css";
import { useNavigate } from "react-router-dom";
import { React, useState, useEffect } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Navbar from "../../components/_navbar";
import Button from "../../components/_button";
import { Link } from "react-router-dom";
import Score from "../../components/_yourScore";
import HelloMessage from "../../components/_helloMessage";

const Dashboard = () => {
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken"]);
  let decoded = "";
  const [users, setUsers] = useState({});

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/user/${decoded.id}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <>
      <Navbar />
      {/*Main Body*/}
      <main className="bg-dark navbar-expand-lg">
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Add Friend
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <form>
                <div className="modal-body">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="username"
                    aria-label="addUser"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Modal */}
        <div className="container-fluid">
          <HelloMessage></HelloMessage>
          <div className="row">
            <div className="my-4 col-md-8 ">
              <Score></Score>
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary text-capitalize">
                      {users.fullname}
                    </div>
                  </div>
                  <hr></hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">{users.email}</div>
                  </div>
                  {/* <hr></hr> */}
                </div>
              </div>
              <div className="container-form-btn">
                <div className="wrap-form-btn w-auto">
                  <div className="form-bgbtn"></div>
                  <Link to="/dashboard/profile/edit">
                    <Button
                      className="form-btn"
                      text="Edit information"
                      color="black"
                    ></Button>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-md-3 offset-md-1">
              <div className="bg-grey card h-50 border-3 text-bg-dark">
                <img
                  className="rounded-circle image-fluid  h-25 mx-auto d-block my-3"
                  src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png"
                  alt="profile pic.jpg"
                />
                <div className="wy-2 text-center fs-1">{decoded.fullname}</div>
                <hr />
                <ul className="px-2 list-unstyled">
                  <li>
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#onlineCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="onlineCollapse"
                    >
                      <i className="bi bi-dot text-success fs-3" /> Online{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="onlineCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row">
                          <li className="small fw-bold my-1 px-1">Friend 1</li>
                          <li className="small fw-bold my-1 px-1">Friend 2</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li className="wy-2 fw-bold px-2">
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#chatCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="chatCollapse"
                    >
                      <i className="bi bi-chat-dots me-2" /> Chat{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="chatCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row"></ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <img className="img-fluid w-50" src="" alt="pubg header" />
            </div>
          </div>
        </div>
      </main>
      {/*Main Body*/}
    </>
  );
};

export default Dashboard;
