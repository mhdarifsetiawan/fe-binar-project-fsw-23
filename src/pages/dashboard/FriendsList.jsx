import React, { useState, useEffect } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/_navbar";
import { useParams } from "react-router-dom";
import jwt_decode from "jwt-decode";
import Score from "../../components/_yourScore";
import "../../styles/friendslist.css";
import HelloMessage from "../../components/_helloMessage";

const FriendsList = () => {
  const [cookies, setCookies] = useCookies(["accessToken"]);
  let decoded = "";
  const { id } = useParams();
  const [countOne, setCountOne] = useState(1);
  const [countTwo, setCountTwo] = useState(2);
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    }
  }, []);

  const fetchUsers = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/users?limitParm=10&pageNumber=1`
      )
      .then((res) => {
        const friendsList = res.data;
        setUsers(friendsList);
      })
      .catch((err) => console.error(err));
  };

  console.log(users);

  const addCounting = (countOne, countTwo, countThree) => {
    setCountOne(countOne + 1);
    setCountTwo(countTwo + 1);
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/users?pageNumber=${countTwo}`
      )
      .then((res) => {
        const friendsList = res.data;
        setUsers(friendsList);
      })
      .catch((err) => console.error(err));
  };

  const lessCounting = (countOne, countTwo) => {
    if (countOne > 1) {
      setCountOne(countOne - 1);
      setCountTwo(countTwo - 1);
      axios
        .get(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/users?pageNumber=${countTwo - 2}`
        )
        .then((res) => {
          const friendsList = res.data;
          setUsers(friendsList);
        })
        .catch((err) => console.error(err));
    }
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <div>
      <Navbar></Navbar>
      <main className="bg-dark navbar-expand-lg">
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Add Friend
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <form>
                <div className="modal-body">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="username"
                    aria-label="addUser"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Modal */}
        <div className="container-fluid">
          <HelloMessage></HelloMessage>
          <div className="row">
            <div className="my-4 col-md-8 ">
              <Score></Score>
              <div className="friendslist-wrapper d-flex">
                <div className="row">
                  {users.map((user) => {
                    return (
                      <div className="col-12 friendslist-row">
                        <div className="friendslist-poto"></div>
                        <div className="friendslist-name-wrapper">
                          <div className="friendslist-name">
                            {user.fullname}
                          </div>
                          <div className="friendslist-email">{user.email}</div>
                          <div className="friendslist-score">
                            Last score : {user.score}
                          </div>
                        </div>
                        {/* <div className="friendslist-score">{user.score}</div> */}
                      </div>
                    );
                  })}
                </div>
              </div>
              {/*  */}
              <div
                className="container pagination"
                style={{
                  height: "40px",
                  justifyContent: "center",
                  marginTop: "20px",
                }}
              >
                <center>
                  <nav aria-label="Page navigation example">
                    <ul class="pagination">
                      <li class="page-item">
                        <a
                          class="page-link"
                          href="#"
                          aria-label="Previous"
                          onClick={() => lessCounting(countOne, countTwo)}
                        >
                          <span aria-hidden="true">&laquo;</span>
                        </a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">
                          {countOne}
                        </a>
                      </li>
                      <li class="page-item">
                        <a
                          class="page-link"
                          href="#"
                          aria-label="Next"
                          onClick={() => addCounting(countOne, countTwo)}
                        >
                          <span aria-hidden="true">&raquo;</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </center>
              </div>
              {/*  */}
            </div>
            <div className="col-md-3 offset-md-1">
              <div className="bg-grey card h-50 border-3 text-bg-dark">
                <img
                  className="rounded-circle image-fluid  h-25 mx-auto d-block my-3"
                  src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png"
                  alt="profile pic.jpg"
                />
                <div className="wy-2 text-center fs-1">{decoded.fullname}</div>
                <hr />
                <ul className="px-2 list-unstyled">
                  <li>
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#onlineCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="onlineCollapse"
                    >
                      <i className="bi bi-dot text-success fs-3" /> Online{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="onlineCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row">
                          <li className="small fw-bold my-1 px-1">Friend 1</li>
                          <li className="small fw-bold my-1 px-1">Friend 2</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li className="wy-2 fw-bold px-2">
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#chatCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="chatCollapse"
                    >
                      <i className="bi bi-chat-dots me-2" /> Chat{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="chatCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row"></ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default FriendsList;
