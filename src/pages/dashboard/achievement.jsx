import axios from "axios";
import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/_navbar";
import Badge00 from "../../images/badges/00_badge.jpg";
import Badge01 from "../../images/badges/01_badge.jpg";
import Badge02 from "../../images/badges/02_badge.jpg";
import Badge03 from "../../images/badges/03_badge.jpg";
import Badge04 from "../../images/badges/04_badge.jpg";
import Badge05 from "../../images/badges/05_badge.jpg";
import Badge06 from "../../images/badges/06_badge.jpg";

/*Coded By Arya*/

const Achievement = () => {
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken", "userId","fullname"]);
  const [score, setScore] = useState(0);
  const images = [Badge00,Badge01,Badge02,Badge03,Badge04,Badge05,Badge06]

  useEffect(() => {
    if (!cookies.accessToken) {
      console.log(cookies);
      alert("Invalid Token");
      navigate("/login");
    } else {
      axios
        .get(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/user/${cookies.userId}`,
          {
            headers: { Authorization: `Bearer ${cookies.accessToken}` },
          }
        )
        .then((res) => {
          setScore(res.data.score);
        })
        .catch((error) => console.log(error));
    }
  }, []);

  return (
    <>
      <Navbar />
      <main>
        <center>
          <h1 className="text-bg-dark my-3">Achievement</h1>
          <h2 className='fw-bold text-bg-dark my-4 '>Player : {cookies.fullname}</h2>
        </center>
        <div className="container-fluid row">
          <div className="card bg-dark text-bg-dark col-md-4 mb-3">
          <img src={images[0]}/>
            <h4 className="card-header fw-bold">The Adventurer</h4>
            <p>Register and Login an account </p>
          </div>
          <div className={score > 0 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[1]}/>
          <h4 className="card-header fw-bold">Everything needs a start</h4>
            <p>Play and gain a point</p>
          </div>
          <div className={score > 11 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[2]}/>
          <h4 className="card-header fw-bold">The Novice</h4>
            <p>You're starting to know some useful skill</p>
          </div>
        </div>
        <div className="container-fluid row">
          <div className={score > 25 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[3]}/>
          <h4 className="card-header fw-bold">The Grinder</h4>
            <p>You're starting to gain your pace.</p>
          </div>
          <div className={score > 50 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[4]}/>
          <h4 className="card-header fw-bold">Intermediate Grinder</h4>
            <p>Now you're just pushing yourself.</p>
          </div>
          <div className={score > 100 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[5]}/>
          <h4 className="card-header fw-bold">Too much grind</h4>
            <p>You're grinding too much that you're known as god grinder.</p>
          </div>
        </div>
        <div className="container-fluid row">
          <div className={score > 200 ? "card bg-dark text-bg-dark col-md-4 mb-3" : "card bg-dark text-bg-dark col-md-4 mb-3 opacity-0"}>
          <img src={images[6]}/>
          <h4 className="card-header fw-bold">Godlike</h4>
            <p>You're now recorded in history as the god among man.</p>
          </div>
        </div>
      </main>
    </>
  );
};

export default Achievement;
