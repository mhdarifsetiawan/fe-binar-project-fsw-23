import { React, useState, useEffect } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import jwt_decode from "jwt-decode";
import Navbar from "../../components/_navbar";
import Button from "../../components/_button";
import { Link } from "react-router-dom";
import Score from "../../components/_yourScore";
import HelloMessage from "../../components/_helloMessage";

const EditProfile = () => {
  const [values, setValues] = useState({});
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken"]);
  let decoded = "";

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/user/${decoded.id}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setValues(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .put(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/user/${decoded.id}`,
        values,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => navigate("/dashboard/profile"))
      .catch((err) => {
        alert("Invalid data");
      });
  };

  return (
    <div>
      <Navbar />
      {/*Main Body*/}
      <main className="bg-dark navbar-expand-lg">
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Add Friend
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <form>
                <div className="modal-body">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="username"
                    aria-label="addUser"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Modal */}
        <div className="container-fluid">
          <HelloMessage></HelloMessage>
          <div className="row">
            <div className="my-4 col-md-8 ">
              <Score></Score>
              <div className="form-edit-profile">
                <form onSubmit={handleSubmit}>
                  <div className="mb-3 text-white">
                    <label for="fullname" className="form-label">
                      Full Name
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="fullname"
                      name="fullname"
                      aria-describedby="fullname Help"
                      value={values.fullname}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="mb-3 text-white">
                    <label for="email" className="form-label">
                      Email address
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      aria-describedby="emailHelp"
                      value={values.email}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="container-form-btn">
                    <div className="wrap-form-btn">
                      <div className="form-bgbtn"></div>
                      <Button className="form-btn" text="Edit"></Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="col-md-3 offset-md-1">
              <div className="bg-grey card h-50 border-3 text-bg-dark">
                <img
                  className="rounded-circle image-fluid  h-25 mx-auto d-block my-3"
                  src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png"
                  alt="profile pic.jpg"
                />
                <div className="wy-2 text-center fs-1">{decoded.fullname}</div>
                <hr />
                <ul className="px-2 list-unstyled">
                  <li>
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#onlineCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="onlineCollapse"
                    >
                      <i className="bi bi-dot text-success fs-3" /> Online{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="onlineCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row">
                          <li className="small fw-bold my-1 px-1">Friend 1</li>
                          <li className="small fw-bold my-1 px-1">Friend 2</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li className="wy-2 fw-bold px-2">
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#chatCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="chatCollapse"
                    >
                      <i className="bi bi-chat-dots me-2" /> Chat{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="chatCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row"></ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <img className="img-fluid w-50" src="" alt="pubg header" />
            </div>
          </div>
        </div>
      </main>
      {/*Main Body*/}
    </div>
  );
};

export default EditProfile;
