import axios from "axios";
import { React } from "react";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import Button from "../components/_button";

import "../styles/main.css";
import "../styles/registration.css";
import "../styles/form.css";
import "../styles/_button.css";

const Forgot = () => {
  const [values, setValues] = useState({});
  const navigate = useNavigate();

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/user/forgot-password",
        values
      )
      .then((res) => {        
          const data = res.data;
          console.log(res);        
          if (res.status === 200) {
            alert(data.message);
            navigate("/login");
          }       
        })
      .catch((err) => {
        const data = err.response;          
        if (data.status === 400) {
          alert(data.data.message);         
        }        
      });
  };

  return (
    <div className="content">
      <div className="registration-navbar">
        <nav className="navbar navbar-expand-lg bg-light">
          <div className="container-fluid">
            <Link className="navbar-brand" to="/">
              Titik Koma
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/registration">
                    Registration
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/login">
                    Login
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-4 m-auto bg-white p-4">
            <span className="form-title">Forgot Password Form</span>
            <form onSubmit={handleSubmit}>             
              <div className="mb-3">
                <label for="email" className="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  aria-describedby="emailHelp"
                  required
                  onChange={handleChange}
                />                
              </div>           
              <div className="container-form-btn">
                <div className="wrap-form-btn">
                  <div className="form-bgbtn"></div>
                  <Button className="form-btn" text="Submit"></Button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Forgot;
