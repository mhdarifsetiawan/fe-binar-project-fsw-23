import { React, useState, useEffect, useCallback } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useNavigate, Link } from "react-router-dom";
import Button from "../../components/_button";
import { LoginSocialGoogle } from "reactjs-social-login";
import { GoogleLoginButton } from "react-social-login-buttons";
import "../../styles/registration.css";
import "../../styles/form.css";

const REDIRECT_URI = window.location.href;

const Login = () => {
  const [values, setValues] = useState({});
  const [cookies, setCookies] = useCookies(["accessToken"]);
  const navigate = useNavigate();
  const [provider, setProvider] = useState("");
  const [profile, setProfile] = useState();

  const onLoginStart = useCallback(() => {
    alert("Login with Google Account");
  }, []);

  useEffect(() => {
    if (provider === "google") {
      try {
        axios
          .post(
            (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
              "/user/registration",
            {
              fullname: profile.name,
              email:
                profile.name.replace(/\s/g, "").toLowerCase() + "@gmailacc.com",
              password:
                process.env.REACT_APP_GG_PASS_ADD +
                profile.name.replace(/\s/g, "").toLowerCase(),
            }
          )
          .then(console.log("login created with google auth"))
          .catch((err) => console.log(err));
      } catch (error) {
        console.log("user has been created before");
      }
      axios
        .post(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            "/auth/login",
          {
            email:
              profile.name.replace(/\s/g, "").toLowerCase() + "@gmailacc.com",
            password:
              process.env.REACT_APP_GG_PASS_ADD +
              profile.name.replace(/\s/g, "").toLowerCase(),
          }
        )
        .then((res) => {
          const { accessToken, id, email } = res.data;
          setCookies("accessToken", accessToken, { maxAge: 3600 });
          setCookies("userId", id, { maxAge: 3600 });
          setCookies("email", email, { maxAge: 3600 });
          navigate("/dashboard");
        })
        .catch((err) => {
          alert("Invalid login");
        });
    }
    if (cookies.accessToken) {
      return navigate("/dashboard");
    }
  });
  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/auth/login",
        values
      )
      .then((res) => {
        const { accessToken, id, email, fullname } = res.data;
        setCookies("accessToken", accessToken, { maxAge: 3600 });
        setCookies("userId", id, { maxAge: 3600 });
        setCookies("email", email, { maxAge: 3600 });
        navigate("/dashboard");
      })
      .catch((err) => {
        alert("Invalid login");
      });
  };

  return (
    <div className="content">
      <div className="login-navbar">
        <nav className="navbar navbar-expand-lg bg-light">
          <div className="container-fluid">
            <Link className="navbar-brand" to="/">
              Titik Koma
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/registration">
                    Registration
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/login">
                    Login
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-4 m-auto bg-white p-4">
            <span className="form-title">Login Form</span>
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label for="email" className="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  aria-describedby="emailHelp"
                  onChange={handleChange}
                />
                {/* <div id="emailHelp" class="form-text">
                  We'll never share your email with anyone else.
                </div> */}
              </div>
              <div className="mb-3">
                <label for="password" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  onChange={handleChange}
                />
              </div>
              <div className="container-form-btn">
                <div className="wrap-form-btn">
                  <div className="form-bgbtn"></div>
                  <Button className="form-btn" text="Login"></Button>
                </div>
              </div>
            </form>
            <div className="mt-2">
              <LoginSocialGoogle
                client_id={process.env.REACT_APP_GG_APP_ID || ""}
                onLoginStart={onLoginStart}
                onResolve={({ provider, data }) => {
                  setProvider(provider);
                  // setCookies("accessToken", data.access_token, {
                  //   maxAge: 3600,
                  // });
                  // setCookies("provider", provider, { maxAge: 3600 });
                  // setCookies("fullname", data.name, { maxAge: 3600 } )
                  setProfile(data);
                }}
                onReject={(err) => {
                  console.log(err);
                }}
              >
                <GoogleLoginButton />
              </LoginSocialGoogle>
            </div>
            <div className="col text-center mt-2">
              <Link className="nav-link" to="/forgot-password">
                Forgot password?
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
