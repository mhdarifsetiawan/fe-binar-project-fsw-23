import { React, useEffect, useState } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/_navbar";
import jwt_decode from "jwt-decode";
import Button from "../components/_button";
import { Link } from "react-router-dom";
import "../../src/styles/leaderboard.css";
import Score from "../components/_yourScore";
import HelloMessage from "../components/_helloMessage";

const Leaderboard = () => {
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken"]);
  let decoded = "";

  const [leaders, setLeaders] = useState([]);
  const fetchLeaders = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/users?pageNumber=1&limitParm=10"
      )
      .then((res) => {
        const listLeaders = res.data;
        setLeaders(listLeaders);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    fetchLeaders();
  }, []);

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    }
  }, []);
  return (
    <div>
      <Navbar></Navbar>
      <main className="bg-dark navbar-expand-lg">
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Add Friend
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                />
              </div>
              <form>
                <div className="modal-body">
                  <input
                    className="form-control"
                    type="search"
                    placeholder="username"
                    aria-label="addUser"
                  />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Modal */}
        <div className="container-fluid">
          <HelloMessage></HelloMessage>
          <div className="row">
            <div className="my-4 col-md-8 ">
              <Score></Score>
              <div className="leaderboard-wrapper text-white">
                <div className="table-leaderboard">
                  <div className="main-row-leaderboard">
                    <div className="row">
                      <div className="col-4">
                        <span>Player Name</span>
                      </div>
                      <div className="col-4 ranking-wrapper">
                        <span>Ranking</span>
                      </div>
                      <div className="col-4 score-wrapper">
                        <span>Score</span>
                      </div>
                    </div>
                  </div>
                  <hr></hr>
                  {leaders.map((leader, index) => {
                    return (
                      <div className="row-leaderboard">
                        <div className="row">
                          <div className="col-4 fs-5 text-capitalize">
                            <span>{leader.fullname}</span>
                          </div>
                          <div
                            className={
                              index === 0
                                ? "col-4 fs-3 ranking-wrapper"
                                : index === 1
                                ? "col-4 fs-4 ranking-wrapper"
                                : index === 2
                                ? "col-4 fs-5 ranking-wrapper"
                                : "col-4 fs-6 ranking-wrapper"
                            }
                          >
                            <span
                              className={
                                index === 0
                                  ? "rounded-circle bg-danger ranking"
                                  : index === 1
                                  ? "rounded-circle bg-warning ranking"
                                  : index === 2
                                  ? "rounded-circle bg-primary ranking"
                                  : "rounded-circle bg-dark ranking"
                              }
                            >
                              {index + 1}
                            </span>
                          </div>
                          <div className="col-4 fs-4 fw-bold score-wrapper">
                            <span className="score">{leader.score}</span>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="col-md-3 offset-md-1">
              <div className="bg-grey card h-50 border-3 text-bg-dark">
                <img
                  className="rounded-circle image-fluid  h-25 mx-auto d-block my-3"
                  src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png"
                  alt="profile pic.jpg"
                />
                <div className="wy-2 text-center fs-1">{decoded.fullname}</div>
                <hr />
                <ul className="px-2 list-unstyled">
                  <li>
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#onlineCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="onlineCollapse"
                    >
                      <i className="bi bi-dot text-success fs-3" /> Online{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="onlineCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row">
                          <li className="small fw-bold my-1 px-1">Friend 1</li>
                          <li className="small fw-bold my-1 px-1">Friend 2</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li className="wy-2 fw-bold px-2">
                    <div
                      className="small fw-bold my-3"
                      data-bs-toggle="collapse"
                      href="#chatCollapse"
                      role="button"
                      aria-expanded="false"
                      aria-controls="chatCollapse"
                    >
                      <i className="bi bi-chat-dots me-2" /> Chat{" "}
                      <i className="bi bi-chevron-down" />
                    </div>
                    <div className="collapse" id="chatCollapse">
                      <div className="card card-body bg-dark">
                        <ul className="navbar-nav row"></ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <img className="img-fluid w-50" src="" alt="pubg header" />
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Leaderboard;
