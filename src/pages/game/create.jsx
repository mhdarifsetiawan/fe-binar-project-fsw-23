import React, { useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "../../styles/creategame.css";
import Navbar from "../../components/_navbar";

const CreateGame = (props) => {
  const navigate = useNavigate();
  const [roomData, setRoomData] = useState({ name: "", description: "" });
  const [cookies] = useCookies("accessToken");

  const handleChange = (e) => {
    setRoomData({ ...roomData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "/game/createGame",
        roomData,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        const roomId = res.data.id;
        alert("Room Created successfully. Will redirect you to the room.");
        navigate(`/game/${roomId}`);
      })
      .catch((err) => {
        alert("Room Creation Failed.");
        console.log(err);
      });
  };

  return (
    <>
      <div>
        <Navbar />
        <main>
          <form
            className="col-md-12 my-4 text-bg-dark fw-bold"
            onSubmit={handleSubmit}
          >
            <center>
              <h1>Create New Game Room</h1>
              <hr className="col-md-8 my-3" />
            </center>

            <div className="bg-create-form col-md-8 offset-md-2">
              <div className="my-3 px-5">
                <div>
                  <label>
                    <h3>Room Name</h3>
                  </label>
                </div>
                <div div className="my-1">
                  <input
                    className="fillbox"
                    onChange={handleChange}
                    name="name"
                    placeholder="My Room"
                  />
                </div>
              </div>
              <div className="my-3 px-5">
                <div>
                  <label>
                    <h3>Description</h3>
                  </label>
                </div>
                <div div className="my-1">
                  <input
                    className="fillbox"
                    onChange={handleChange}
                    name="description"
                    placeholder="My Room Description"
                  />
                </div>
              </div>
              <div className="button-create-margin">
                <button className="button-submit-create" type="submit">
                  Create this room
                </button>
              </div>
            </div>
          </form>
        </main>
      </div>
    </>
  );
};

export default CreateGame;
