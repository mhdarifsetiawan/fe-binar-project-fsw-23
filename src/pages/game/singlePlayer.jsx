import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import "../../styles/creategame.css";
import Navbar from "../../components/_navbar";

/* Coded By Arya */

const SinglePlayer = (props) => {
  const navigate = useNavigate();
  const description = {
    number: {
      game: "Number Guessing Game",
      description:
        "Play a simple number guessing game, train you deduction skill",
    },
    hangman: {
      game: "Hangman",
      description: "Play the popular Word Guessing Game",
    },
    empty: {
      game: "-",
      description: "-",
    },
    rps: {
        game: "Rock Paper Scissor with COM",
        description: "Test your rock paper scissor skills with computer before challenging other player online.",
      },
  };
  const [game, setgame] = useState(null);
  const [cookies] = useCookies("accessToken");

  const handleChange = (e) => {
    setgame(e.target.value);
  };

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/dashboard");
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (game !== null && game != "empty") {
      navigate(`/game/single/${game}`);
    } else {
      alert("Please pick a game.");
    }
  };

  return (
    <>
      <div>
        <Navbar />
        <main>
          <form
            className="col-md-12 my-4 text-bg-dark fw-bold"
            onSubmit={handleSubmit}
          >
            <center>
              <h1>Pick a Single Player Game</h1>
              <hr className="col-md-8 my-3" />

              <select
                style={styleList.select}
                name="gameName"
                onChange={handleChange}
              >
                <option value="empty"> ---Game Name--- </option>
                <option value="number">Number Guessing Game</option>
                <option value="hangman">Hangman</option>
                <option value="rps">Rock Paper Scissor vs COM</option>
              </select>
            </center>
            {game ? (
              <>
                <div style={styleList.box}>
                  <div>Game Name :</div>
                  <h3 className="text-warning">{description[game].game}</h3>
                  <div>Description</div>
                  <h4 className="text-primary">
                    {description[game].description}
                  </h4>
                </div>
              </>
            ) : (
              <>
                <div style={styleList.box}>
                  <div>Game Name :</div> <h3 className="text-warning">-</h3>
                  <div>Description</div>
                  <h4 className="text-primary">-</h4>
                </div>
              </>
            )}
            <center>
              <button
                className="button-submit-create"
                type="submit"
                style={styleList.button}
              >
                Play choosen game
              </button>
            </center>
          </form>
        </main>
      </div>
    </>
  );
};

const styleList = {
  box: {
    width: "75%",
    display: "block",
    margin: "auto",
    border: "2px solid white",
    padding: "5px",
    borderRadius: '8px',
  },
  select: { minWidth: "250px", minHeight: "2em", marginBottom: "3em" },
  button: {
    width: "40%",
    marginTop: "3em",
    backgroundColor: "limegreen",
  },
};

export default SinglePlayer;
