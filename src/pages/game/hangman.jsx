import { useEffect, useState } from "react";
import Navbar from "../../components/_navbar";
import { useCookies } from "react-cookie";
import axios from "axios";
import '../../styles/hangman.css'
import Picture0 from '../../images/hangman/00.jpg'
import Picture1 from '../../images/hangman/01.jpg'
import Picture2 from '../../images/hangman/02.jpg'
import Picture3 from '../../images/hangman/03.jpg'
import Picture4 from '../../images/hangman/04.jpg'
import Picture5 from '../../images/hangman/05.jpg'
import Picture6 from '../../images/hangman/06.jpg'

/*Coded By Arya*/

const Hangman = () => {
    const images = [Picture0,Picture1,Picture2,Picture3,Picture4,Picture5,Picture6];
    const [cookies] = useCookies(["accessToken", "userId"]);
  const alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];
  const wordList = [
    "time",
    "jump",
    "trap",
    "monkey",
    "horse",
    "duck",
    "juice",
    "america",
    "pickle",
    "continue",
    "peanuts",
    "lady",
    "sydney",
    "pumpkin",
    "computer",
    "phyton",
    "ruby",
    "java",
    "javascript",
  ];
  const answer = wordList[Math.floor(Math.random() * wordList.length)];
  const [life, setLife] = useState(6);
  const [word, setWord] = useState([])
  const [form, setForm] = useState([]);
  const [win,setWin] = useState(false);
  const [once,setOnce] = useState(false);
  const [picture,setPicture] = useState(0)
  const [playState, setPlayState] = useState(true);
  

  const checkAnswer = (alpha) => {
    const form1 = [...form]
    let isIn = false;
    for (let i = 0; i < form.length; i++) {
      if (word[i] == alpha) {
        form1[i]=alpha;
        isIn = true;
      }
    }
    if(!isIn){
        setLife(life-1);
        setPicture(picture+1);
    }
    setForm(form1)
  };

  const checkWin = () => {
    if (word == form.join('')){
        setWin(true);
        setPlayState(false);
        console.log('win');
        axios
          .post(
            (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
              `/user/addScore/${cookies.userId}`,
            {},
            { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
          )
          .then((res) => console.log("Game Ended"))
          .catch((error) => console.log(error));
      }
      else if(life < 1){
        setPlayState(false)
      }
    }

  const handleClick = (e) => {
    checkAnswer(e.target.value);
    e.target.classList.add('invisible-hangman')
  };

  const handleReset = () => {
    window.location.reload();
  }

  useEffect(() => {
    if (once != true){
        const blank = [];
        for (let i = 0; i < answer.length; i++) {
          blank.push("_");
        }
        setForm(blank);
        setWord(answer);
        setOnce(true)
    } else {
        checkWin();
    }
  }, [form,win,playState]);

  return playState ? (
    <>
      <Navbar />
      <main>
          <center>
          <div className='text-bg-dark'>
          <h1>Hangman Game</h1>
          </div>
          <div className='hangman-border my-3'>
          <img className='hangman-img' src={images[picture]} />
          </div>
          <div className='fs-1 text-bg-dark form-hangman-spacing'>
          {form}
          </div>
            <div className="text-bg-dark my-4">
            <div class="container-fluid">
            {alphabet.map((alph) => {
                return (
                  <>
                    <button className='btn btn-outline-danger' value={`${alph}`} onClick={handleClick}>
                      {alph}
                    </button>
                  </>
                );
              })}
            </div>
              <h2 className='my-3'>Your Life {life}</h2>
            </div>
          </center>
      </main>
    </>
  ):(
    <>
      <Navbar />
      <center>
        <main className="text-bg-dark">
        <div className='text-bg-dark'>
          <h1>Hangman Game</h1>
          </div>
          <div className='hangman-border'>
          <img className='hangman-img' src={images[picture]} />
          </div>
          <h1 className="my-5">
            The Game has ended and you {win ? "win" : "lose"}.{" "}
          </h1>
          <button className='btn btn-outline-warning' onClick={handleReset}>New Game</button>
        </main>
      </center>
    </>
  );;
};

export default Hangman;
