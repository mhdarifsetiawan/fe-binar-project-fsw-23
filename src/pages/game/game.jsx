import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import { useCookies } from "react-cookie";
import jwt_decode from "jwt-decode";
import Navbar from "../../components/_navbar";
import "../../styles/game.css";
import {
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon,
} from "react-share";

/*Coded By Arya*/

const Game = () => {
  const shareUrl = `https://titik-koma-game-website.herokuapp.com/`;
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken"]);
  const { roomId } = useParams("roomId");
  const decoded = jwt_decode(cookies.accessToken);
  const [userChoice, setUserChoice] = useState("");
  const [player, setPlayer] = useState("");
  const [gameStatus, setGameStatus] = useState(null);

  useEffect(() => {
    if (!roomId) {
      alert("The room does not exist.");
      navigate("/dashboard");
    }
    __fetchGameStatus();
  }, []);

  const __fetchGameStatus = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/game/${roomId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        const gamePlay = res.data;
        if (gamePlay.userId1 === null) {
          setPlayer("player1");
        } else {
          setPlayer("player2");
        }
        setGameStatus(gamePlay);
      })
      .catch((error) => console.log(error));
  };

  const handleClick = (e) => {
    e.preventDefault();
    setUserChoice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (gameStatus.userId1 === decoded.id) {
      alert(
        "You have choosen an option. Please wait till the other users pick their option"
      );
      navigate("/dashboard");
    } else {
      axios
        .put(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/game/play/${gameStatus.id}`,
          { player: player, userId: decoded.id, userChoice: userChoice },
          { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
        )
        .then((res) => {
          window.location.reload();
        })
        .catch((error) => console.log(error));
    }
  };

  return gameStatus ? (
    <>
      <Navbar />
      <main>
        <div className="text-bg-dark">
          <center>
            <h1>Multiplayer Rock Paper Scissor Game</h1>
            <div className=" my-4 translucent-background">
              <div>
                <h3 className="text-uppercase fw-bold my-3">
                  {gameStatus.name}
                </h3>
              </div>
              <div>
                <h4 className="my-4">{gameStatus.description}</h4>
              </div>
              <div>
                <p>Choose your option</p>
              </div>
              <div className="rounded">
                <form onSubmit={handleSubmit}>
                  <button
                    className="rounded-circle bg-light fw-bolder width-heigth-100"
                    value="R"
                    onClick={handleClick}
                  >
                    👊
                  </button>
                  <button
                    className="rounded-circle bg-primary fw-bolder width-heigth-100"
                    value="P"
                    onClick={handleClick}
                  >
                    ✋️
                  </button>
                  <button
                    className="rounded-circle bg-warning fw-bolder width-heigth-100"
                    value="S"
                    onClick={handleClick}
                  >
                    ✌️
                  </button>
                  <div>
                    <h4 className="my-3">
                      <span className="text-warning">
                        {userChoice === "P" ? (
                          <>
                            You have choosen : <br />
                            Paper
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              ✋️
                            </span>
                          </>
                        ) : userChoice === "R" ? (
                          <>
                            Your have choosen : <br />
                            Rock
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              👊
                            </span>
                          </>
                        ) : userChoice === "S" ? (
                          <>
                            Your have choosen : <br />
                            Scissor
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              ✌️
                            </span>
                          </>
                        ) : (
                          ""
                        )}
                      </span>
                    </h4>
                  </div>
                  {gameStatus.winner === null ? (
                    <button type="submit" className="btn btn-warning my-3">
                      I Have choosen my Option
                    </button>
                  ) : (
                    <></>
                  )}
                </form>
                {gameStatus.winner !== null ? (
                  <>
                    <h2>This room winner is {gameStatus.winner}</h2>
                    <button
                      className="btn btn-success text-bg-dark my-4"
                      onClick={() => {
                        alert("Directing to create page.");
                        navigate("/create");
                      }}
                    >
                      Create New Room
                    </button>
                    <div>
                      <h5>Share it :</h5>
                      <FacebookShareButton
                        url={shareUrl}
                        quote={
                          "Lets play the game rock paper scissor with me, and other single player game at Titik Koma"
                        }
                        hashtag={"#titikkoma,#rps,#letsplay"}
                      >
                        <FacebookIcon size={42} round />
                      </FacebookShareButton>
                      <TwitterShareButton
                        title={"Titik Koma game page"}
                        url={shareUrl}
                      >
                        <TwitterIcon size={42} round />
                      </TwitterShareButton>
                    </div>
                    <br />
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </center>
        </div>
      </main>
    </>
  ) : (
    <>
      <center>
        <h1>Loading....</h1>
      </center>
    </>
  );
};

export default Game;
