import { useState, useEffect } from "react";
import Navbar from "../../components/_navbar";
import axios from "axios";
import "../../styles/numberGuess.css";
import { useCookies } from "react-cookie";

/* Coded By Arya */

const NumberGuessing = () => {
  const [cookies] = useCookies(["accessToken", "userId"]);
  const [life, setLife] = useState(7);
  const [answer, setAnswer] = useState(null);
  const [guess, setGuess] = useState();
  const [playState, setPlayState] = useState(true);
  const [win, setWin] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    setAnswer(Math.floor(Math.random() * 100) + 1);
  }, []);

  const handleChange = (e) => {
    setGuess(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (guess != null && typeof parseInt(guess) === "number") {
      if (answer == guess) {
        setPlayState(false);
        setMessage("You are right");
        alert(`You win the game with ${life-1} chance(s) left`);
        setWin(true);
        axios
          .post(
            (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
              `/user/addScore/${cookies.userId}`,
            {},
            { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
          )
          .then((res) => console.log("Game Ended"))
          .catch((error) => console.log(error));
      } else if (answer != guess) {
        setLife(life - 1);
        answer > guess
          ? setMessage("The answer is higher number")
          : setMessage("The answer is lower number");
      }
      if (life < 3 && !win) {
        console.log("lose");
        setPlayState(false);
        setMessage("Sorry but you lost you chances, You lost this game.");
      }
    } else {
      alert("Please input an answer.");
    }
  };

  const handleReset = () => {
    window.location.reload();
  }

  return playState ? (
    <>
      <Navbar />
      <center>
        <main className="text-bg-dark">
          <div className="my-4 number-background">
            <h3 className="my-4">
              Let's play a game where you guess a Number which is between
            </h3>
            <h2 className="my-3 fw-bold text-danger">1 to 100</h2>
            <h5>
              You have 6 chance to guess the answer before you chances run out.
            </h5>
            <h3 className="my-3">Your chances : {life - 1}</h3>
            <div>
              <form onSubmit={handleSubmit}>
                <input
                  name="guess"
                  className="input-number-size"
                  onChange={handleChange}
                  placeholder="1~100"
                ></input>
                <div>
                  <button className="form-number-btn" type="submit">
                    {" "}
                    Guess
                  </button>
                </div>
              </form>
              <div className="my-3 footer-number text-warning fs-4">
                {message}
              </div>
            </div>
          </div>
        </main>
      </center>
    </>
  ) : (
    <>
      <Navbar />
      <center>
        <main className="text-bg-dark">
          <h1 className="my-5">
            The Game has ended and you {win ? "win" : "lose"}.{" "}
          </h1>
          <button className='btn btn-outline-warning' onClick={handleReset}>New Game</button>
        </main>
      </center>
    </>
  );
};

export default NumberGuessing;
