import axios from "axios";
import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/_navbar";
import "../../styles/game.css";

/*Coded By Arya*/

const Rps = () => {
  const [choice, setChoice] = useState("");
  const [cookies] = useCookies(["accessToken", "userId"]);
  const [userChoice, setUserChoice] = useState("");
  const [gameStatus, setGameStatus] = useState(true);
  const [com, setCom] = useState("");
  const [win, setWin] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    setChoice(`${com}${userChoice}`);
    setGameStatus(false);
  };

  const checkWinner = () => {
    if (choice === "31" || choice === "23" || choice === "12") {
      setWin(true);
      axios
        .post(
          (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            `/user/addScore/${cookies.userId}`,
          {},
          { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
        )
        .then((res) => console.log("Game Ended"))
        .catch((error) => console.log(error));
    } else if (choice === "13" || choice === "32" || choice === "21") {
      setWin(false);
    }
  };

  const handleReset = () => {
    window.location.reload();
  };

  useEffect(() => {
    if (gameStatus === false) {
      checkWinner();
    } else if (gameStatus === true) {
      setCom(Math.floor(Math.random() * 3) + 1);
    }
  }, [choice]);

  const handleClick = (e) => {
    e.preventDefault();
    setUserChoice(e.target.value);
  };

  return gameStatus ? (
    <>
      <Navbar />
      <main>
        <div className="text-bg-dark">
          <center>
            <h1>Multiplayer Rock Paper Scissor Game</h1>
            <div className=" my-4 translucent-background">
              <div>
                <h3 className="text-uppercase fw-bold my-3">
                  {gameStatus.name}
                </h3>
              </div>
              <div>
                <h4 className="my-4">{gameStatus.description}</h4>
              </div>
              <div>
                <p>Choose your option</p>
              </div>
              <div className="rounded">
                <form onSubmit={handleSubmit}>
                  <button
                    className="rounded-circle bg-light fw-bolder width-heigth-100"
                    value="1"
                    onClick={handleClick}
                  >
                    👊
                  </button>
                  <button
                    className="rounded-circle bg-primary fw-bolder width-heigth-100"
                    value="2"
                    onClick={handleClick}
                  >
                    ✋️
                  </button>
                  <button
                    className="rounded-circle bg-warning fw-bolder width-heigth-100"
                    value="3"
                    onClick={handleClick}
                  >
                    ✌️
                  </button>
                  <div>
                    <h4 className="my-3">
                      <span className="text-warning">
                        {userChoice === "2" ? (
                          <>
                            You have choosen : <br />
                            Paper
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              ✋️
                            </span>
                          </>
                        ) : userChoice === "1" ? (
                          <>
                            Your have choosen : <br />
                            Rock
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              👊
                            </span>
                          </>
                        ) : userChoice === "3" ? (
                          <>
                            Your have choosen : <br />
                            Scissor
                            <br />
                            <span className="fw-bolder width-heigth-100">
                              ✌️
                            </span>
                          </>
                        ) : (
                          ""
                        )}
                      </span>
                    </h4>
                  </div>
                  {gameStatus === true ? (
                    <button type="submit" className="btn btn-warning my-3">
                      I Have choosen my Option
                    </button>
                  ) : (
                    <></>
                  )}
                </form>
              </div>
            </div>
          </center>
        </div>
      </main>
    </>
  ) : (
    <>
      <Navbar />
      <main>
        <div className="text-bg-dark">
          <center>
            <h1>Single Player Rock Paper Scissor Game</h1>
            <div className=" my-4 translucent-background">
              <h2 className="my-4">
                This winner is{" "}
                {win === true ? "Player" : win === false ? "COM" : "Draw"}
              </h2>
              <div>
                <h4 className="my-3">
                  <span className="text-warning">
                    {userChoice === "2" ? (
                      <>
                        You have choosen : <br />
                        Paper
                        <br />
                        <span className="fw-bolder width-heigth-100">✋️</span>
                      </>
                    ) : userChoice === "1" ? (
                      <>
                        You have choosen : <br />
                        Rock
                        <br />
                        <span className="fw-bolder width-heigth-100">👊</span>
                      </>
                    ) : userChoice === "3" ? (
                      <>
                        You have choosen : <br />
                        Scissor
                        <br />
                        <span className="fw-bolder width-heigth-100">✌️</span>
                      </>
                    ) : (
                      ""
                    )}
                  </span>
                </h4>
              </div>
              <div>
                <h4 className="my-3">
                  <span className="text-warning">
                    {com === 2 ? (
                      <>
                        COM has choosen : <br />
                        Paper
                        <br />
                        <span className="fw-bolder width-heigth-100">✋️</span>
                      </>
                    ) : com === 1 ? (
                      <>
                        COM has choosen : <br />
                        Rock
                        <br />
                        <span className="fw-bolder width-heigth-100">👊</span>
                      </>
                    ) : com === 3 ? (
                      <>
                        COM has choosen : <br />
                        Scissor
                        <br />
                        <span className="fw-bolder width-heigth-100">✌️</span>
                      </>
                    ) : (
                      ""
                    )}
                  </span>
                </h4>
              </div>
              <button className="btn btn-outline-warning" onClick={handleReset}>
                New Game
              </button>
            </div>
          </center>
        </div>
      </main>
    </>
  );
};

export default Rps;
