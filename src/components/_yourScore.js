import { React, useEffect, useState } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";

const Score = () => {
  const navigate = useNavigate();
  const [cookies] = useCookies(["accessToken"]);
  const [score, setScore] = useState([]);
  let decoded = "";

  const fetchScore = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/user/${decoded.id}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setScore(res.data.score);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchScore();
  }, []);

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate("/login");
    }
  }, []);

  return (
    <div className="mb-4">
      <div className="circle-box position-relative top-50 start-40">
        <p className="mb-0 fw-bold">Your</p>
        <p className="fw-bold">Score</p>
        <p className="fw-bolder fs-2">{score}</p>
      </div>
    </div>
  );
};

export default Score;
