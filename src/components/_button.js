import "../styles/_button.css";

const Button = (props) => {
  const { text = "Submit", className = "", color = "" } = props;

  return (
    <button className={className} style={{ color: color }}>
      {text}
    </button>
  );
};

export default Button;
