import React from "react";
import { Link } from "react-router-dom";
import { useCookies } from "react-cookie";

const _navbarLanding = () => {
  const [cookies] = useCookies(["accessToken"]);
  let decoded = "";
  return (
    <div className="nav-landing">
      <nav
        class="navbar navbar-expand-lg navbar-dark fixed-top"
        style={{ minHeight: "70px", backgroundColor: "rgba(0, 0, 0, 0.5)" }}
      >
        <div class="container-fluid">
          <Link className="navbar-brand" to="/">
            Titik Koma
          </Link>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li
                className="nav-item"
                style={{ display: !cookies.accessToken ? "none" : "" }}
              >
                <Link className="nav-link" to="/dashboard">
                  Dashboard
                </Link>
              </li>
              <li
                class="nav-item"
                style={{ display: cookies.accessToken ? "none" : "" }}
              >
                <Link className="nav-link" to="/registration">
                  Registration
                </Link>
              </li>
              <li
                class="nav-item"
                style={{ display: cookies.accessToken ? "none" : "" }}
              >
                <Link className="nav-link" to="/login">
                  Login
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default _navbarLanding;
