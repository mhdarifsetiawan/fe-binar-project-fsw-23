import { React, useState, useEffect } from "react";
import axios from "axios";
import { useCookies } from "react-cookie";
import jwt_decode from "jwt-decode";

const HelloMessage = () => {
  const [users, setUsers] = useState({});
  const [cookies] = useCookies(["accessToken"]);
  let decoded = "";

  try {
    decoded = jwt_decode(cookies.accessToken);
  } catch (error) {
    console.log(error);
  }

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          `/user/${decoded.id}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      <div className="col-md-12 text-bg-dark">
        <h3 className=" text-muted fw-bold">Dashboard</h3>
      </div>
      <h3 className="ms-3 fw-2 text-warning text-capitalize">
        Hello, {users.fullname}
      </h3>
    </div>
  );
};

export default HelloMessage;
