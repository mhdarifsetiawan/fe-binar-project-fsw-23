import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";

const Navbar = () => {
  const [cookies, setCookies, removeCookie] = useCookies(["userId"]);
  const navigate = useNavigate();
  const logout = () => {
    const text = "are you sure want to logout?";
    if (window.confirm(text) == true) {
      removeCookie(["accessToken"]);
      removeCookie(["userId"]);
      removeCookie(["email"]);
      removeCookie(["fullname"]);
      navigate("/login");
    }
  };
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container-fluid">
          {/* offcanvas toggler*/}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasWithBothOptions"
            aria-controls="offcanvasWithBothOptions"
          >
            <span
              className="navbar-toggler-icon"
              data-bs-target="#offcanvasWithBothOptions"
            />
          </button>
          {/* offvanvas toogler*/}
          <a className="navbar-brand fw-bold text-uppercase me-2">
            GameWebPage
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <form className="d-flex ms-auto" role="search">
              <input
                className="form-control"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button className="btn btn-outline-primary" type="submit">
                <i className="bi bi-search" />
              </button>
            </form>
            <ul className="navbar-nav mb-2 mb-lg-0">
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  // href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="bi bi-person" />
                </a>
                <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <a className="dropdown-item">Settings</a>
                  </li>
                  <li>
                    <a className="dropdown-item">Top Up</a>
                  </li>
                  <li>
                    <a
                      style={{ cursor: "pointer" }}
                      className="dropdown-item"
                      onClick={logout}
                    >
                      Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {/* offCanvas*/}
      <div>
        <div
          className="offcanvas text-bg-dark offcanvas-start sidebar-nav active"
          data-bs-scroll="true"
          tabIndex={-1}
          id="offcanvasWithBothOptions"
          aria-labelledby="offcanvasWithBothOptionsLabel"
        >
          <div className="offcanvas-body p-0">
            <nav className="navbar-dark">
              <ul className="navbar-nav">
                <li>
                  <div className="text-muted small fw-bold text-uppercase px-2">
                    Link
                  </div>
                </li>
                <li>
                  <div className="fw-bold px-3 active my-2">
                    <Link
                      to="/dashboard"
                      style={{ color: "white", textDecoration: "none" }}
                    >
                      <span className="me-2">
                        <i className="bi bi-speedometer" />
                      </span>
                      Dashboard
                    </Link>
                  </div>
                </li>
                <li className="my-1">
                  <hr />
                </li>
                <li>
                  <div className="text-muted small fw-bold text-uppercase px-2">
                    Game
                  </div>
                </li>
                <li>
                  <div
                    className="small fw-bold px-3 my-3"
                    data-bs-toggle="collapse"
                    href="#gameCollapse"
                    role="button"
                    aria-expanded="false"
                    aria-controls="gameCollapse"
                  >
                    <i className="bi bi-joystick me-3" /> Games{" "}
                    <i className="bi bi-chevron-down" />
                  </div>
                  <div className="collapse" id="gameCollapse">
                    <div className="card card-body bg-dark">
                      <ul className="navbar-nav">
                        <li className="small fw-bold my-1 px-3">
                          <Link
                            to="/game/single"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <i className="bi bi-person" /> Single Player Game
                          </Link>
                        </li>
                        <li className="small fw-bold my-1 px-3">
                          <Link
                            to="/create"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <i className="bi bi-dice-3" /> Create New Room
                          </Link>
                        </li>
                        <li className="small fw-bold my-2 px-3">
                          <Link
                            to="/game/list"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <i className="bi bi-door-open" /> Room List
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="fw-bold px-3 active my-2">
                    <Link
                      to="/leaderboard"
                      style={{ color: "white", textDecoration: "none" }}
                    >
                      <span className="me-2">
                        <i className="bi bi-trophy" />
                      </span>
                      Leaderboard
                    </Link>
                  </div>
                </li>
                <li>
                  <Link
                    to="/achievement"
                    style={{ color: "white", textDecoration: "none" }}
                  >
                    <div className="fw-bold px-3 active my-2">
                      <span className="me-2">
                        <i className="bi bi-shield-shaded" />
                      </span>{" "}
                      Achievement
                    </div>
                  </Link>
                </li>
                <li className="my-1">
                  <hr />
                </li>
                <li>
                  <div className="text-muted small text-uppercase fw-bold px-2">
                    User
                  </div>
                </li>
                <li>
                  <div
                    className="small fw-bold px-3 my-3"
                    data-bs-toggle="collapse"
                    href="#profileCollapse"
                    role="button"
                    aria-expanded="false"
                    aria-controls="profileCollapse"
                  >
                    <i className="bi bi-person-circle me-2" /> Profile{" "}
                    <i className="bi bi-chevron-down" />
                  </div>
                  <div className="collapse" id="profileCollapse">
                    <div className="card card-body bg-dark">
                      <ul className="navbar-nav">
                        <li className="small fw-bold my-1 px-3">
                          <Link
                            to="/dashboard/profile"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <span className="me-2">
                              <i className="bi bi-eye" />
                            </span>
                            View Profile
                          </Link>
                        </li>
                        <li className="small fw-bold my-1 px-3">
                          <Link
                            to="/dashboard/profile/edit"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <i className="bi bi-pencil-square me-2" /> Edit
                            Profile
                          </Link>
                        </li>
                        <li className="small fw-bold my-2 px-3">
                          <Link
                            to="/dashboard/password/edit"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            <i className="bi bi-key-fill me-2" /> Change
                            Password
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
                <li>
                  <div
                    className="small fw-bold px-3 my-2"
                    data-bs-toggle="collapse"
                    href="#friendCollapse"
                    role="button"
                    aria-expanded="false"
                    aria-controls="friendCollapse"
                  >
                    <i className="bi bi-people me-2" /> Friends{" "}
                    <i className="bi bi-chevron-down" />
                  </div>
                  <div className="collapse" id="friendCollapse">
                    <div className="card card-body bg-dark">
                      <ul className="navbar-nav">
                        <li className="small fw-bold my-1 px-3">
                          <i className="bi bi-person-lines-fill me-2" />
                          <Link
                            to="/dashboard/friendlist"
                            style={{ color: "white", textDecoration: "none" }}
                          >
                            Friend List
                          </Link>
                        </li>
                        {/* Button trigger modal */}
                        <li
                          className="small fw-bold my-2 px-3"
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModal"
                          type="button"
                        >
                          <i className="bi bi-person-plus-fill me-2" />
                          Add new friend
                        </li>
                        {/* Button trigger modal */}
                      </ul>
                    </div>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
